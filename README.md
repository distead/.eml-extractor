# README #

### WHAT IS THE PURPOSE OF .EML EXTRACTOR? ###

The main purpose of .eml extractor is to scan a disk image to see if any .eml files are present. An eml file is a file type used by some email clients when saving a local copy of an email.
If .eml extractor detects an .eml file, it will then proceed to extract the file into a specific folder according to the file name of the disk image). The .eml files will then also be scanned to see if any base64 content-transfer-encoding is present, this means the .eml file had an encoded attachment attached to it. The attachment will then be decoded and extracted alongside its parent .eml file.


### HOW TO USE .EML EXTRACTOR IN WINDOWS ###

* Select the folder in which eml_extractor.py is located (eml_extractor/eml_extractor.py)
* [Shift + Right-Click] to open up the command line window.
* Type: eml_extractor.py
* Follow the prompt to input the file path for the disk image you wish to analyse.

**Note:**

- The extracted files will appear in a folder within the same location as the disk image file.
- .eml files with large attachments will currently take a long time to process.


### KNOWN ISSUES ###

* The extractor does not extract non-encoded or non-base64 encoded attachments.
* The extractor is currently unable to correctly determine if more than 1 attachment is present on a single .eml file, and extract them individually.
* The specific 'extracted' files folder needs to be removed before re-running the same disk image.


### CONTACT ###

Bitbucket user: distead