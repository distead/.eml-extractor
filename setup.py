from distutils.core import setup

setup(name='eml_extractor',
      version='1.0',
      description='EML File Extractor',
      author='Danielle Istead',
      author_email='d.istead@cranfield.ac.uk',
      url='https://bitbucket.org/distead/.eml-extractor/overview',
      packages=['eml_extractor'])