import logging
import os

import base64_decoder


logging.basicConfig(format="%(asctime)s: %(levelname)s - %(message)s", filename="extract_eml_attachment.log",
                    filemode="w", level=logging.DEBUG)


def extract_eml_attachment(folder_path, eml_file):
    function_passed = True
    eml_file_path = ""

    try:
        eml_file_path = os.path.join(folder_path, eml_file)
        function_passed = os.path.isfile(eml_file_path)
    except TypeError:
        function_passed = False

    if function_passed and not eml_file_path == "":
        logging.debug("Accessed eml file: " + eml_file_path)

        eml_file_pointer = open(eml_file_path, "r")
        logging.debug("Opened eml file: " + eml_file_path)

        eml_file_data = eml_file_pointer.read()
        logging.debug("Read eml file: " + eml_file_path)

        eml_file_split = eml_file_data.split("\n")

        # Variables that will be extracted from the file.
        eml_boundary = ""
        attachment_filename = ""
        attachment_start_line = 0
        attachment_end_line = 0

        for line_index, each_line in enumerate(eml_file_split):
            if each_line.find('boundary="') > -1:  # To determine the boundary within the email.
                if eml_boundary == "":
                    eml_boundary = each_line.strip()
                    eml_boundary = eml_boundary.split('"')[1]
                    logging.debug("Eml file boundary: " + eml_boundary)

            if each_line.find('boundary=') > -1:  # To determine the boundary within the email.
                if eml_boundary == "":
                    eml_boundary = each_line.strip()
                    eml_boundary = eml_boundary.split('=')[1]
                    logging.debug("Eml file boundary: " + eml_boundary)

            # To determine if content is attached to the email.
            if (each_line.lower()).find("content-disposition:") > -1:
                logging.info("An attachment present within eml file: " + eml_file_path)

            if each_line.find('filename="') > -1:  # To determine the original attachment filename.
                attachment_filename = each_line.strip()
                attachment_filename = attachment_filename.split('"')[1]
                logging.debug("Attachment filename: " + attachment_filename)

                for i in range(line_index, len(eml_file_split)):
                    if eml_file_split[i] == "":
                        attachment_start_line = i + 1
                        print("\nLocated: " + attachment_filename + " - From: " + eml_file)
                        break

            if each_line.find(eml_boundary + "--") > -1:  # To determine the last boundary.
                attachment_end_line = line_index

        if attachment_filename != "":
            extracted_attachment_list = eml_file_split[attachment_start_line:attachment_end_line]
            extracted_attachment = "\n".join(extracted_attachment_list)

            if extracted_attachment == "":
                function_passed = False

            # Decode attachment - encode ascii converts to string to bytes :
            # http://stackoverflow.com/questions/17615414/how-to-convert-binary-string-to-normal-string-in-python3
            extracted_attachment = base64_decoder.base64_decode_string(extracted_attachment.encode('ascii'))
            if extracted_attachment == "TYPE_ERR":
                function_passed = False

            if function_passed:
                attachment_output = os.path.join(folder_path, eml_file_path[:-4] + "_" + attachment_filename)
                attachment_output_file_pointer = open(attachment_output, "wb")
                logging.debug("Attachment output file: " + attachment_output)

                attachment_output_file_pointer.write(extracted_attachment)
                logging.debug("Encoded attachment written to file: " + attachment_output)
                print("Decoded file: " + attachment_output)

                attachment_output_file_pointer.close()

        eml_file_pointer.close()

    return function_passed

# if __name__ == "__main__":
#     extract_eml_attachment("D:\\University\\Cranfield University\\Programming for Forensic Science"
#                            "\\Test\\TestEmailAccountInfo", "emailattachment.eml")
#     print()
#
#     extract_eml_attachment("D:\\University\\Cranfield University\\Programming for Forensic Science"
#                            "\\Test\\TestEmailAccountInfo", "zip folder attachment 1.eml")