import base64
import os
import logging

logging.basicConfig(format="%(asctime)s: %(levelname)s - %(message)s",
                    filename="base64_decoder.log", filemode="w", level=logging.DEBUG)


def base64_decode_string(input_string):
    decoded_data = b""

    try:
        decoded_data = base64.b64decode(input_string)

    except TypeError:
        decoded_data = "TYPE_ERR"

    return decoded_data


def base64_decode_file(input_folder_path, encoded_input_file):
    encoded_input_file_path = os.path.join(input_folder_path, encoded_input_file)

    logging.debug("Accessed file: " + encoded_input_file_path)
    print("Decoding file: " + encoded_input_file_path)

    encoded_input_file_pointer = open(encoded_input_file_path, "rb")
    logging.debug("Opened input file: " + encoded_input_file_path)

    encoded_input_file_data = encoded_input_file_pointer.read()
    logging.debug("File read: " + encoded_input_file_path)

    decoded_data = base64.b64decode(encoded_input_file_data)
    logging.debug("Decoded input file: " + encoded_input_file_path)

    output_file_name = "decoded_" + encoded_input_file
    decoded_output_file_path = os.path.join(input_folder_path, output_file_name)

    decoded_output_file_pointer = open(decoded_output_file_path, "wb")
    logging.debug("Output file created: " + decoded_output_file_path)
    print("Decoded output file: " + decoded_output_file_path)

    decoded_output_file_pointer.write(decoded_data)
    logging.debug("Decoded file data written to output file: " + decoded_output_file_path)
    print("Decoding complete.")

    encoded_input_file_pointer.close()
    logging.debug("Input file closed: " + encoded_input_file_path)

    decoded_output_file_pointer.close()
    logging.debug("Output file closed: " + decoded_output_file_path)

# if __name__ == "__main__":
#     base64_decoder("D:\\University\\Cranfield University\\Programming for Forensic Science"
#                    "\\Test\\TestEmailAccountInfo", "gathering.zip")
#     print()
#     base64_decoder("D:\\University\\Cranfield University\\Programming for Forensic Science"
#                    "\\Test\\TestEmailAccountInfo", "myVMfolder.zip")
