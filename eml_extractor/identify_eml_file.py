import logging
import re
import os
import sys

logging.basicConfig(format="%(asctime)s: %(levelname)s - %(message)s",
                    filename="identify_eml_file.log", filemode="w", level=logging.DEBUG)


def identify_eml_file_header(filename):
    disk_image_file_pointer = open(filename, "rb")
    logging.debug("Opened file: " + filename)

    eml_file_header = b"\x52\x65\x74\x75\x72\x6E\x2D\x50\x61\x74\x68\x3A\x20"

    disk_image_file_header = disk_image_file_pointer.read(13)
    logging.debug("Read file: " + filename)

    find_eml_results = re.match(eml_file_header, disk_image_file_header)
    if find_eml_results:
        logging.debug("Matched file header in: " + filename)

        print(find_eml_results)
        print("Start byte: " + str(find_eml_results.start()))
        print("End byte: " + str(find_eml_results.end()))
        print("Matched phrase: " + str(find_eml_results.group()))
        print()

    else:
        logging.debug("No eml file header identified in: " + filename)
        print("No eml file header was identified.")


def extract_eml_from_disk_image(disk_image_folder, disk_image_filename):
    disk_image_file = os.path.join(disk_image_folder, disk_image_filename)
    disk_image_file_pointer = open(disk_image_file, "rb")
    logging.debug("Opened disk image file: " + disk_image_filename)

    sector_size = 512
    list_of_eml_start_sectors = []  # *by 512 to get file byte position.
    current_sector_offset = 0

    print("-- Locating .eml headers --\n")

    while True:
        current_sector_data = disk_image_file_pointer.read(sector_size)
        current_file_header = current_sector_data[:26]
        logging.debug("Read in header data for current sector offset: " + str(current_sector_offset))

        if re.match(b"(\x52\x65\x74\x75\x72\x6E\x2D\x50\x61\x74\x68\x3A\x20|"
                    b"\x44\x65\x6C\x69\x76\x65\x72\x65\x64\x2D\x54\x6F\x3A)", current_file_header):
            logging.info("Identified eml header at sector: " + str(current_sector_offset))
            print("Identified eml header at sector: " + str(current_sector_offset))
            list_of_eml_start_sectors.append(current_sector_offset)

        current_sector_offset += 1

        if len(current_sector_data) < sector_size:
            logging.debug("The end of the disk file has been reached.")
            break

    print("\n-- " + str(len(list_of_eml_start_sectors)) + " .eml headers were located on the disk image --\n")

    #  Folder path of extracted eml files.
    extracted_folder_path = r""

    print("\n-- Extracting located .eml files --")

    for eml_file_number, eml_file_header_location in enumerate(list_of_eml_start_sectors):
        disk_image_file_pointer.seek(512 * eml_file_header_location)
        current_eml_data = b""

        print("\nFile " + str(eml_file_number))

        while True:
            read_1_byte = disk_image_file_pointer.read(1)  # Reading 1 byte at a time.
            current_eml_data += read_1_byte

            # Current size of eml data.
            eml_data_size = sys.getsizeof(current_eml_data)

            sys.stdout.write("\rCurrent Size: {0} bytes".format(eml_data_size))
            sys.stdout.flush()

            if re.match(b"\x00", current_eml_data[-1:]):
                extracted_folder_path = os.path.join(disk_image_folder, disk_image_filename[:-4] + "_extracted")
                if not os.path.exists(extracted_folder_path):
                    os.makedirs(extracted_folder_path)

                extracted_eml_file = os.path.join(extracted_folder_path, "eml_file_" +
                                                  str(eml_file_number) + ".eml")
                extracted_eml_file_pointer = open(extracted_eml_file, "wb")
                logging.debug("Opened eml export file: " + extracted_eml_file)

                extracted_eml_file_pointer.write(current_eml_data)
                logging.debug("Extracted eml file to: " + extracted_eml_file)

                extracted_eml_file_pointer.close()
                logging.debug("Closed eml export file: " + extracted_eml_file)

                break

    print("\n\n-- Extracted all .eml files to: " + extracted_folder_path + " --")
    print()

    disk_image_file_pointer.close()
    logging.debug("Disk image file pointer closed.")

    return extracted_folder_path


# if __name__ == "__main__":
#     # identify_eml_file_header("D:\\University\\Cranfield University\\Programming for Forensic Science\\"
#     #                          "Test\\TestEmailAccountInfo\\emailattachment.eml")
#     # print()
#
#     extract_eml_from_disk_image("D:\\University\\Cranfield University\\Programming for Forensic Science"
#                                 "\\Test\\TestEmailAccountInfo", "testvhd.vhd")
