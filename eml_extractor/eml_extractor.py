####################
# Imported Modules #
####################
import logging
import os

import identify_eml_file
import extract_eml_attachment

logging.basicConfig(format="%(asctime)s: %(levelname)s - %(message)s",
                    filename="eml_extractor.log", filemode="w", level=logging.DEBUG)


def validate_file_path(file_path):
    return os.path.isfile(file_path)

if __name__ == '__main__':
    ##########################
    # User Input - File Path #
    ##########################
    user_input = ""

    while not validate_file_path(user_input):
        user_input = input("Please enter the file location of your disk image. ")

    disk_image_file_path = os.path.dirname(os.path.realpath(user_input))
    disk_image_file_name = os.path.basename(user_input)

    #######################
    # The Analysis Begins #
    #######################
    print("Beginning analysis of: " + disk_image_file_name)
    print()

    extracted_folder_path = identify_eml_file.extract_eml_from_disk_image(disk_image_file_path, disk_image_file_name)
    eml_file_list = os.listdir(extracted_folder_path)

    print("\n-- Obtaining .eml attachments --")

    for each_eml in eml_file_list:
        extraction_passed = extract_eml_attachment.extract_eml_attachment(extracted_folder_path, each_eml)
        if extraction_passed:
            print("\nFinished analysing: " + each_eml)
        else:
            print("\nFailed to analyse: " + each_eml)

    print("\nAnalysis complete.")

    #######
    # END #
    #######

#  [TEST] D:\University\Cranfield University\Programming for Forensic Science\Test\TestEmailAccountInfo\testvhd.vhd