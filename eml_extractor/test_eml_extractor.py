import unittest
import os

from eml_extractor import validate_file_path
from identify_eml_file import extract_eml_from_disk_image
from extract_eml_attachment import extract_eml_attachment
from base64_decoder import base64_decode_string


test_path = "D:\\University\\Cranfield University\\Programming for Forensic Science\\Test\\TestEmailAccountInfo\\"
test_file = "testvhd.vhd"


class TestingUserInput(unittest.TestCase):

    # Testing extract_eml_from_disk_image from identify_eml_file.
    def test_extracting_eml_files(self):
        self.assertNotEqual(extract_eml_from_disk_image(test_path, test_file), "")

    # Testing validate_file_path from eml_extractor.
    def test_file_path_input(self):
        self.assertFalse(validate_file_path(""))
        self.assertFalse(validate_file_path(1), msg="An int has been entered.")
        self.assertFalse(validate_file_path(b""), msg="A binary string has been entered.")
        self.assertFalse(validate_file_path(os.path.join(b"a", b"a")))

        self.assertTrue(validate_file_path("D:\\University\\Cranfield University\\Programming for Forensic Science"
                                           "\\Test\\TestEmailAccountInfo\\testvhd.vhd"))

    # Testing eml files.
    def test_eml_attachment_extraction(self):
        self.assertFalse(extract_eml_attachment("a", 1))
        self.assertFalse(extract_eml_attachment(24, 78))
        self.assertFalse(extract_eml_attachment(1, "a"))
        self.assertFalse(extract_eml_attachment(b"a", b"a"))
        self.assertFalse(extract_eml_attachment("", ""))
        self.assertFalse(extract_eml_attachment("\\", "a"))

        self.assertTrue(extract_eml_attachment("D:\\University\\Cranfield University\\Programming for Forensic Science"
                                               "\\Test\\TestEmailAccountInfo\\testvhd_extracted\\", "eml_file_0.eml"))

    # Testing base64 decoder.
    def test_base64_decoder(self):
        self.assertEqual(base64_decode_string(1581348), "TYPE_ERR")
        self.assertNotEqual(base64_decode_string("aGVsbG8="), "TYPE_ERR")

if __name__ == '__main__':
    unittest.main()